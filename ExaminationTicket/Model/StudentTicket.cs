﻿using System.Runtime.Serialization;

namespace ExaminationTicket.Model
{
    /// <summary>
    /// Экзамеционный билет
    /// </summary>
    [DataContract]
    class StudentTicket
    {
        public StudentTicket(string fio, Questions questions)
        {
            Fio = fio;
            QuestionsList = questions;
        }

        /// <summary>
        /// ФИО студента
        /// </summary>
        [DataMember]
        public string Fio { get; private set; }

        /// <summary>
        /// Вопросы студента
        /// </summary>
        [DataMember]
        public Questions QuestionsList { get; private set; }
    }
}