﻿using System.Runtime.Serialization;

namespace ExaminationTicket.Model
{
    [DataContract]
    public class Setting
    {
        public Setting(string number, string patchFile)
        {
            Number = number;
            PatchFile = patchFile;
        }

        /// <summary>
        ///     Номер файла (вопроса в билете)
        /// </summary>
        [DataMember]
        public string Number { get; set; }

        /// <summary>
        ///     Путь к файлу с вопросами
        /// </summary>
        [DataMember]
        public string PatchFile { get; set; }
    }
}