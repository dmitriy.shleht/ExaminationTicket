﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using ExaminationTicket.GUI;
using ExaminationTicket.Services;

namespace ExaminationTicket.Model
{
    [DataContract]
    internal class Examination
    {
        [DataMember]
        private int lastTicket;

        public Examination()
        {
            Read();
        }

        [DataMember]
        public StudentsTickets StudentsTickets { get; private set; }

        [DataMember]
        public Questions QuestionsAll { get; private set; }

        /// <summary>
        ///     Вытянуть случайный билет
        /// </summary>
        /// <param name="fio">ФИО</param>
        /// <returns>Билет</returns>
        public StudentTicket PullTicket(string fio)
        {
            var questions = new Questions();

            foreach (var item in QuestionsAll.GroupBy(x => x.Number))
            {
                if (lastTicket == item.Count())
                    throw new Exception("Экзаменационные билеты закончились!");

                questions.Add(item.ToList()[lastTicket]);
            }

            questions.Sort();

            var studentTicket = new StudentTicket(fio, questions);

            StudentsTickets.Add(studentTicket);

            lastTicket++;

            Write();

            return studentTicket;
        }

        /// <summary>
        ///     Сохранение сессии экзамена в репозиторий
        /// </summary>
        private void Write()
        {
            var serializer = new Serializer<Examination>(MainForm.PatchExamination);
            serializer.WriteObjectToJson(this);
        }

        /// <summary>
        ///     Восстановление сессии экзамена из репозитория
        /// </summary>
        private void Read()
        {
            if (File.Exists(MainForm.PatchExamination))
            {
                var serializer = new Serializer<Examination>(MainForm.PatchExamination);
                var examination = serializer.ReadObjectFromJson();

                QuestionsAll = examination.QuestionsAll;
                StudentsTickets = examination.StudentsTickets;
                lastTicket = examination.lastTicket;
            }
            else
                StudentsTickets = new StudentsTickets();
        }

        /// <summary>
        ///     Новый экзамен и инициализация вопросов из файлов
        /// </summary>
        public void NewExamination()
        {
            File.Delete(MainForm.PatchExamination);
            QuestionsAll?.Clear();
            StudentsTickets?.Clear();
            lastTicket = 0;

            var questions = new Questions();

            //загружаем настройки из файла
            var serializer = new Serializer<List<Setting>>(MainForm.PatchSettings);

            //Читаем перемешанные вопросы
            questions.ReadFilesRandom(serializer.ReadObjectFromJson());

            QuestionsAll = questions;
        }
    }
}